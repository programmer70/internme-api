/*
 This file is for only Assets Route on website like ---> Following, unfollow, like,
 unlike, comment, uncomment and more assets releted functions ....... 
 This only private API ... use only those users who are login
 @route :- /api/assets
 @access :- PRIVET
*/
const express = require('express');
const { userFollowing, userFollower, userUnFollowing, userUnFollower } = require('../controller/AssetsController');


const route = express.Router();

route.post('/follow',userFollowing,userFollower);
route.post('/ufollow',userUnFollowing,userUnFollower);

module.exports = route;