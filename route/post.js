/*
@dec = Candidates Post
@access = PRIVATE
@path = /api/CandidatePost
*/
// import libraries 
const express = require('express');
const { updatefile } = require('../controller/fileController/updatefile');
const { uploadFile } = require('../controller/fileController/uploadfile');
const { CreatPost, userDinfo } = require('../controller/PostController/CreatPost');
const { addComment, showComments } = require('../controller/PostController/MediaActivity(like,comment,share)/commnet');
const { PostLike } = require('../controller/PostController/MediaActivity(like,comment,share)/like');
const { showAllpost, showPostById, showPostDparams } = require('../controller/PostController/ShowPost');
const { UpdatePost } = require('../controller/PostController/UpdatePost');
const { verifyToken } = require('../helper/doubleVerifyAuthuser');

const postrouter = express.Router();

// routing 
//file handle route
//1. new
postrouter.post('/new/imgUpload/:id-:token/:filetype/:profileorpost', uploadFile)
postrouter.post('/new/videoUpload/:id-:token/:filetype/:profileorpost', uploadFile)
postrouter.post('/new/docUpload/:id-:token/:filetype/:profileorpost', uploadFile)
 
//1. update
postrouter.post('/update/imgUpload/:id-:token/:filetype/:profileorpost', updatefile)
postrouter.post('/update/videoUpload/:id-:token/:filetype/:profileorpost', updatefile)
postrouter.post('/update/docUpload/:id-:token/:filetype/:profileorpost', updatefile)

// Creat New post  
postrouter.post('/new-info/:detailId-:role', CreatPost)  // detailId Detail ID
// update post 
postrouter.put('/update-info/:postId-:postrole',UpdatePost)

// media acitivity like comment and share 
// like and rmlike 
postrouter.post('/like/:postId-:postrole/:detailId-:role',PostLike)
// add comment
postrouter.post('/add_comment/:postId-:postrole/:detailId-:role',verifyToken,addComment)
// show comment
postrouter.get('/show_comments/:postId-:postrole',showComments)
// show post 
// all 
postrouter.get('/show/all',showAllpost)
// sing by postId
postrouter.get('/show/:postId-:postrole',showPostById)
// set params
postrouter.param('detailId', userDinfo)
postrouter.param('postId',showPostDparams)
// export router
module.exports = postrouter;
