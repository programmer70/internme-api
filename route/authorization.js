const express = require('express');
const { singup, login, logout } = require('../controller/authorization');
const router = express.Router();

router.post('/singup/:userType',singup);        // user type studen, emloyer or admin
router.post('/login/',login);        // user type studen, emloyer or admin
router.get('/logout',logout);        // user type studen, emloyer or admin

module.exports = router;