const express = require('express');
const { rootFunction } = require('../controller/hellow');
const route = express.Router();

route.get('/root',rootFunction);

module.exports = route;