/*
@dec = employer details
@access = PRIVATE
@path = /api/employerProfile
*/

const express = require('express');
const { isLogin,userInfo,checkUser } = require('../../controller/authorization');
const { updatefile } = require('../../controller/fileController/updatefile');
const { uploadFile } = require('../../controller/fileController/uploadfile');
// const { uploadCanditateImgFile, uploadCanditateDocFile, uploadCanditateFileprofile, updateCanditateFileprofile } = require('../../controller/fileController/profile/canditateFile');
// const { isLoginhr } = require('../../controller/hrController/authorization');
const { newHrDetail,updateHrDetail,showAlluser,showSingleOrgbyName,showSingleuser, showEmployerConnections } = require('../../controller/hrController/profile/hrDetail');
const router = express.Router();
// root path 
// /api/employerProfile 
// routing
//file handle route
//1. new
router.post('/new/imgupload/:id-:token/:filetype/:profileorpost', uploadFile)
router.post('/new/docupload/:id-:token/:filetype/:profileorpost', uploadFile)

//1. update
router.post('/update/imgupload/:id-:token/:filetype/:profileorpost', updatefile)
router.post('/update/docupload/:id-:token/:filetype/:profileorpost',updatefile)

// info user info new store or update
// 1. new
router.post('/new-info/:id-:token',newHrDetail)

// 2. update
router.put('/update-info/:id-:token',isLogin, updateHrDetail)

// show 
router.get('/employers/all', showAlluser)
router.get('/user/:id', showSingleuser)
router.post('/employers/connections', isLogin, showEmployerConnections)
router.get('/userbyname/:orgname-:unid', showSingleOrgbyName)
//check the user 
router.get('/user-check/:id-:token', checkUser)
// params
router.param('id', userInfo);

module.exports = router; 