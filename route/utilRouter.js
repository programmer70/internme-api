/*
  @access --> Privet and Public
  @router --> /api/give 
  @use    --> only when some data use for front-end (like _id give to front and userDetails _id give to front) 
*/
const express = require('express');
const { giveDetailsIdbySingupId } = require('../controller/utilController');

const router = express.Router();

router.post('/detailsidbyuserid',giveDetailsIdbySingupId);



module.exports = router