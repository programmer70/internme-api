const express = require('express');
const { workrole, workroleDelet, education, educationDelet, skill, skillDelet } = require('../../controller/usercontroller/profile/MoreDetail');
const route = express.Router();

// creat
route.post('/workrole',workrole)
route.post('/education',education)
route.post('/skill',skill)
// delete
route.delete('/workrole/:w_id',workroleDelet)
route.delete('/education/:e_id',educationDelet)
route.delete('/skill/:s_id',skillDelet)

module.exports = route