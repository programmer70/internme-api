/*
@dec = Candidates details
@access = PRIVATE
@path = /api/userProfile
*/


const express = require('express');
// const { uploadCanditateImgFile, uploadCanditateDocFile, uploadCanditateFileprofile, updateCanditateFileprofile, uploadFile,updatefile } = require('../../controller/fileController/uploadfile');
const { isLogin, userInfo, checkUser } = require('../../controller/authorization');
const { updatefile } = require('../../controller/fileController/updatefile');
const { uploadFile } = require('../../controller/fileController/uploadfile');
const { showAlluser, showSingleuser, showSingleuserbyName, updateCanditateDetail, showCanditateConnections } = require('../../controller/usercontroller/profile/userDetail');
const { newCanditateDetail } = require('../../controller/usercontroller/profile/userDetail');
const router = express.Router();

// routing 
//file handle route
//1. new
router.post('/new/imgupload/:id-:token/:filetype/:profileorpost', uploadFile)
router.post('/new/docupload/:id-:token/:filetype/:profileorpost', uploadFile)

//1. update
router.post('/update/imgupload/:id-:token/:filetype/:profileorpost', updatefile)
router.post('/update/docupload/:id-:token/:filetype/:profileorpost', updatefile)

// info canditate info new store or update
// 1. new
router.post('/new-info/:id-:token', newCanditateDetail)

// 2. update
router.put('/update-info/:userId', isLogin, updateCanditateDetail)

// show 
router.get('/candidates/all', showAlluser)
router.get('/user/:id', showSingleuser)
router.post('/candidates/connections', isLogin, showCanditateConnections)
router.get('/userbyname/:fname-:lname-:unid', showSingleuserbyName)

//check the user 
router.get('/user-check/:id-:token', checkUser)
// params
router.param('id', userInfo);

module.exports = router;