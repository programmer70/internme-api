const { kStringMaxLength } = require('buffer');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

const userInfo = new Schema({
    user: {
        type: ObjectId,
        ref: 'User'
    },
    role: {
        type: String,
        default: 'student'
    },
    profileImg: {
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        }
    },
    profileCoverimg: {
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        }
    }
    ,
    firstName: {
        type: String,
        required: true,
        length: {
            min: 3,
            max: 30
        },
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        length: {
            min: 3,
            max: 30
        },
        trim: true
    },
    userName: {
        type: String,
        required: true,
        length: {
            min: 3,
            max: 30
        }
    },
    uniq: String,
    gander: String,
    phone: {
        type: String,
        required: true,
        length: 10,
        trim: true
    },
    languages: {
        type: [String],
        required: true
    },
    headline: {
        type: String,
        trim: true
    },
    about: {
        type: String
    },
    address: {
        // address1:{
        //     type:String,
        //     trim:true,
        //     required:true
        // },
        // landmark: {
        //     type: String,
        //     trim: true,
        //     required: true
        // },
        city: {
            type: String,
            trim: true,
            required: true
        },
        // zip: {
        //     type: String,
        //     trim: true,
        //     require: true
        // },
        state: {
            type: String,
            trim: true,
            required: true
        },
        country: {
            type: String,
            trim: true,
            required: true
        }
    },
    skills: [
        {
            name: String,
            level: String
        }
    ],
    educations: [
        {
            // educationType: String,   /// secudery, higher secudery, deploma, becholer, master,Phd
            status: String,
            school: String,
            degree: String,
            fieldofstudy: String,
            start: Date,
            end: Date,
            grade: String,
            description: {
                length: {
                    max: 250,
                    min: 20
                },
                type: String
            }
        }
    ],

    workrole: [  // experiance 
        {
            experianceType: {
                type: String
            },
            role: {
                type: String
                // required: true
            },
            company: {  // organization name 
                type: String
            },
            city: {
                type: String
            },
            country: {
                type: String
            },
            from: {
                type: Date
            },
            to: {
                type: Date
            },
            current: {
                type: Boolean,
                default: false
            },
            details: {
                type: String
            },
            projectTitle: {
                type: String
            }
        }
    ],
    social: {
        youtube: {
            type: String
        },
        linkedin: {
            type: String
        },
        twitter: {
            type: String
        },
        facebook: {
            type: String
        },
        instagram: {
            type: String

        },
        github: {
            type: String
        }
    },
    workSample: {     /// website name ware user all work stored
        type: String
    }, likedPost: [{
        posts: {
            type: Schema.Types.ObjectId,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            required: true,
            enum: ['CandidatePost', 'EmployerPost']
        }
    }]
    , comments: [{
        type: Schema.Types.ObjectId,
        ref:'comments'
    }],
    connections: [{
        onId: {
            type: Schema.Types.ObjectId,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            enum: ['employerProfile', 'userDetail']
        }
    }],
    cvResume: { // optionl
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        }
    }

}, { timestamps: true });

module.exports = userDetailModel = mongoose.model('userDetail', userInfo)