const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const { ObjectId } = mongoose.Schema;

const employerInfo = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }, role: {
        type: String,
        default: 'employer'
    },
    orgName: {        // organization name
        type: String,
        required: true
    },
    uniq: String,
    phone: {
        type: String,
        required: true,
        length: 10,
        trim: true
    },
    telphone: {
        type: String,
        required: true,
        length: 10,
        trim: true
    },
    address: {
        country: String,
        state: String,
        city: String
    },
    about: {
        type: String
    }, headline: {
        type: String,
        trim: true
    },
    website: {
        type: String
    },
    orgLogo: {       // organization log
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        }
    },
    orgCoverImag: {
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        }
    }
    ,
    otherDocument: {    // for verification 
        fileName: {
            type: String,
            trim: true
        },
        Path: {
            type: String,
            trim: true
        },
        title: {
            type: String,
            trim: true
        },
        somthingAbout: {
            type: String,
            trim: true
        }
    },
    social: {           // for verification 
        youtube: {
            type: String
        },
        facebook: {
            type: String
        },
        instagram: {
            type: String
        },
        linkedin: {
            type: String
        },
        twitter: {
            type: String
        }
    },
    likedPost: [{
        posts: {
            type: Schema.Types.ObjectId,
            required: true,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            required: true,
            enum: ['CandidatePost', 'EmployerPost']
        }
    }], comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comments'
    }],
    connections: [{
        onId: {
            type: Schema.Types.ObjectId,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            enum: ['employerProfile', 'userDetail']
        }
    }]

}, { timestamps: true });

module.exports = hrDetailModel = mongoose.model('employerProfile', employerInfo)