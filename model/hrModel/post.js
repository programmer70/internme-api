const mongoos = require('mongoose'),
    Schema = mongoos.Schema;

const EmployerPostSchema = new Schema({
    postedBy: {
        type: Schema.Types.ObjectId,
        ref: 'employerProfile'
    },
    postedMediaFile: {                  // img, video, or doc 
        photo: {
            fileName: {
                type: String,
                trim: true
            },
            filePath: {
                type: String,
                trim: true
            }
        },
        video: {
            fileName: {
                type: String,
                trim: true
            },
            filePath: {
                type: String,
                trim: true
            }
        },
        documant: {
            fileName: {
                type: String,
                trim: true
            },
            filePath: {
                type: String,
                trim: true
            }
        }
    },
    description: {
        type: String
    },
    postType: {
        type: String,         //Normal Post, internShip,intermidiat,associate,professinal or export
    },
    visible: { type: String },          // anyone , onlye connections , no one
    commentOrnot: { type: String },    // anyone , onlye connections , no one
    role: {
        type: String,
        default: 'employer'
    },
    likes: [{
        user: {
            type: Schema.Types.ObjectId,
            refPath: 'onModel'
        },
        onModel: {
            type: String,
            enum: ['employerProfile', 'userDetail']
        }
    }], comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comments'
    }]
}, { timestamps: true });


module.exports = mongoos.model('EmployerPost', EmployerPostSchema);