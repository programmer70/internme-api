const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const commentSchema = new Schema({
  text: { type: String, required: true },
  post: {
    type: Schema.Types.ObjectId,
    required: true,
    // Instead of a hardcoded model name in `ref`, `refPath` means Mongoose
    // will look at the `onModel` property to find the right model.
    refPath: 'onModel'
  },
  onModel: {
    type: String,
    required: true,
    enum: ['EmployerPost', 'CandidatePost']
  },
  // thread: [
  //   {
  //     body: { type: String, trim: true },
  //     parent_commentId: { type: Schema.Types.ObjectId, ref: 'comments' },
  //     user: {
  //       user: {
  //         type: Schema.Types.ObjectId,
  //         required: true,
  //         refPath: 'userModel'
  //       },
  //       userModel: {
  //         type: String,
  //         required: true,
  //         enum: ['employerProfile', 'userDetail']
  //       }
  //     }
  //   }
  // ],

  user: {
    type: Schema.Types.ObjectId,
    required: true,
    refPath: 'userModel'
  },
  userModel: {
    type: String,
    required: true,
    enum: ['employerProfile', 'userDetail']
  }
});

module.exports = mongoose.model('comments', commentSchema)