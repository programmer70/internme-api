const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
require('dotenv').config();
const port = process.env.PORT || 8080;
// console.log('the port is -> ', port);
const app = express();

// database connections 
mongoose.connect(process.env.MONGO_URI, { useFindAndModify: true, useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
        console.log('DB connected !!');
    })
    .catch(e => {
        console.log('DB not connecte pleas try again -> ', e)

    })
// mongoose.connection.on('error',error => {
//     console.log('Db not connecte please try again !! ->',error);
// })



// import route middelware
// authentication rout 
const Authrouter = require('./route/authorization');
const { isLogin } = require('./controller/authorization');
// details route (create profile,post for employer and student)
const rootRoute = require('./route/mainroot');
const stdprofile = require('./route/userRoute/profile')
// const hrAuthrouter = require('./route/hrRoute/authorization');
const moreDetail = require('./route/userRoute/moreDetail')
const empProfile = require('./route/hrRoute/profile');
// Asset use to handel like follow unfollow and comments and more stuff
const Assets = require('./route/AssetsRoute');
const utillRoute = require('./route/utilRouter');
const postrouter = require('./route/post');
// const hrmoreDetail = require('./route/hrRoute/MoreDetail')

// import static files
app.use(express.static('public'));

// use all middelwere
//module
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}));
//route
app.use(rootRoute);
app.use('/api/userAuth', Authrouter)  // authorization rout
app.use('/api/userProfile', stdprofile) //std -> student /// profile route
app.use('/api/moreDetail', isLogin, moreDetail) /// more details route
// app.use('/api/hrAuth', bodyParser.json(), hrAuthrouter) 
app.use('/api/employerProfile', empProfile)                // emp -> employer /// profile route
// app.use('/api/hrmoreDetail', isLogin,hrmoreDetail )

// post route
app.use('/api/post', isLogin, postrouter)

app.use('/api/assets', isLogin, Assets) //

app.use("/api/give", utillRoute)


// Authorizetion erro handle 
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        return res.status(401).json({
            error: 'Please signin you are not authorized'
        });
    }
});

app.listen(port, () => {
    console.log(`copy this URL -> http://localhost:${port}`);
});