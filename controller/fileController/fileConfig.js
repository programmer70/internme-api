const multer = require('multer');
const uuid = require('uuidv1');
const path = require('path');
const fs = require('fs');
require('dotenv').config;

//  canditate multer storage configration
exports.storageConfig = (fileType, location, extentions) => {
    // // extentions is list and it length is 4
    // console.log('fileType',fileType);
    // console.log('this is storageconfig line 10');
    // console.log('store config -> filname', fileType, ' locations -> ', location, ' extensions -> ', extentions);
    
    config = multer.diskStorage({
        destination: function (req, file, cb) {
            // console.log(location) 
            cb(null, location)
        },
        filename: function (res, file, cb) {
            // console.log('filename -> ',file);
            cb(null, `${uuid()}-${Date.now()}-${file.originalname}`)
        }
        
    })
    var upload = multer({
        storage: config,
        fileFilter: function (req,file,cb){

            var fExt = path.extname(file.originalname).toLocaleLowerCase()
            // console.log('matches line 25 configStrg-> ',extentions.includes(fExt))
            if (extentions.includes(fExt) === false | undefined | null) {
                req.fileValidationError = `Only ${extentions[0]} ,  ${extentions[1]} ,  ${extentions[2]}  and ${extentions[3]}  type img required !!`;
                return cb(new Error(`Only ${extentions[0]} ,  ${extentions[1]} ,  ${extentions[2]}  and ${extentions[3]}  type img required !!`));
            }
            cb(null, true);
        }
    }).single(fileType); // fileType --> image doc video avtart cover otherVerificationDocumnet

    return upload
}

