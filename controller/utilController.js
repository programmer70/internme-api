const userSingupModel = require('../model/person')
const candidateDetailModel = require('../model/caditateModel/ragisterDetail')
const employerDetailModel = require('../model/hrModel/ragisterDetail')

exports.giveDetailsIdbySingupId = (req, res, next) => {
    console.log('req.body ->',req.body); 
    console.log('req.body.userId ->',req.body.userId);
    if (req.body.userId) {
        userSingupModel.findOne({ _id: req.body.userId }).populate('user', '_id  role').exec((error, user) => {
            try {
                if (error || !user) {
                    return res.status(404).json({ error: "User not Found " })
                } else {
                    // console.log("user is -> ", user);
                    if (user.role === 'student') {
                        candidateDetailModel.findOne({ user: { _id: req.body.userId } }).exec((error, userDetails) => {
                            if (error || !user) {
                                return res.status(404).json({ error: "You are not register user please re-login then fill details !!" })
                            } if (user._id) {
                                return res.status(200).json({ userDetailId: userDetails._id })
                            }
                        })
                    } 
                    if (user.role === 'employer') {
                        employerDetailModel.findOne({ user: { _id: req.body.userId } }).exec((error, userDetails) => {
                            if (error || !user) {
                                return res.status(404).json({ error: "You are not register user please re-login then fill details !!" })
                            } if (user._id) {
                                return res.status(200).json({ userDetailId: userDetails._id })
                            }
                        })
                    }
                }
            } catch (error) {
                console.log('error catch -> ', error.message);
            }

        })
    } else {
        return res.status(400).json({ error: "Please give proper details !!" })
    }

}