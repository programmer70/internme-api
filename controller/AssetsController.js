/**
 * This file is for only Assets functions on website like ---> Following, unfollow, like,
 * unlike, comment, uncomment and more assets releted functions ....... 
 * This only private API ... use only those users who are login
*/

const userSingupModel = require('../model/person')
const candidateDetailModel = require('../model/caditateModel/ragisterDetail')
const employerDetailModel = require('../model/hrModel/ragisterDetail')

// Following function  
// the fllower can be any candidate or employer  
exports.userFollowing = async (req, res, next) => {
    /* info
    // req.body.userId ===>  authenticated 
    // req.body.userRole ===> authenticated
    // req.body.followUnFollowId ===> authenticater can Following
    // req.body.followUnFollowRole ===> authenticater can Following
    */
//    console.log('this is following body => ',req.body);
    if (req.body.userId && req.body.userRole && req.body.followUnFollowId && req.body.followUnFollowRole) {
        let userDetailDB; // this veriable use to store user db info on besis of which role (student or employer)

        // let runOrnote; // this vireable use to when follow id check in db    
        if (req.body.userRole === 'student') {
            userDetailDB = candidateDetailModel; // if user is ***student*** 
            // console.log('ok 1');
        } 
        if (req.body.userRole === 'employer') {
            userDetailDB = employerDetailModel; // if user is ***employer*** 
            // console.log('ok 2');
        }

        // check the userId and followUnFollowId  then move farther
        await userDetailDB.findOne({ _id: req.body.userId }).exec((error, user) => {
            try {

                if (error) {
                    console.log(error);
                    res.status(404).json({ error: 'You are not ragister user !!' })
                }
                // console.log('user.connections -> ', user.connections)
                // console.log('user.connections -> ', user.connections.length);

                // check the followUnFollowId is already in db or note  
                if (user.connections.length > 0) {
                    let trueOrfalse = user.connections.map((t, i) => t.onId == req.body.followUnFollowId)

                    // console.log('trueOrfalse1 -> ', trueOrfalse);
                    if (trueOrfalse.includes(true)) {
                        // console.log('trueOrfalse.includes(true) -> ', trueOrfalse.includes(true));
                        // console.log('already follow this user !!');
                        return res.status(208).json({ message: 'You are already follow this user !!' })

                    }

                }
                // if the followUnFollowId is not here then add it 
                if (user.connections.length >= 0) {
                    // console.log('trueOrfalse.includes(false) -> ', trueOrfalse.includes(false));
                    userDetailDB.findByIdAndUpdate(req.body.userId, {
                        $push: {
                            connections: {
                                onId: req.body.followUnFollowId,
                                onModel: req.body.followUnFollowRole === 'employer' ? 'employerProfile' : 'userDetail' // this will take a value as per the followUnFollowRole if followUnFollowRole === 'employer' then onModel = employerProfile elseif followUnFollowRole === 'student' then onModel = userDetail       
                            }
                        }
                    }, (error, user) => {
                        if (error) {
                            return res.status(400).json({
                                error: error
                            })

                        }
                        next();
                    })
                }
            } catch (error) {
                console.log('error -> catch 76 ', error.message);
            }


        });

    } else {
        res.status(417).json({ error: 'Please give proper details !!' })
    }

}

// Follower function  
// the fllower can be any candidate or employer  
exports.userFollower = (req, res, next) => {

    // console.log('followers ',req.body);
    /* info
    // req.body.userId ===>  authenticated 
    // req.body.userRole ===> authenticated
    // req.body.followUnFollowId ===> authenticater can Follower
    // req.body.followUnFollowRole ===> authenticater can Follower
    */
    if (req.body.userId && req.body.userRole && req.body.followUnFollowId && req.body.followUnFollowRole) {
        let userDetailDB; // this veriable use to store user db info on besis of which role (student or employer)
        if (req.body.followUnFollowRole === 'student') {
            userDetailDB = candidateDetailModel; // if user is ***student*** 
        }
        if (req.body.followUnFollowRole === 'employer') {
            userDetailDB = employerDetailModel; // if user is ***employer*** 
        }
        userDetailDB.findByIdAndUpdate(req.body.followUnFollowId, {
            $push: {
                connections: {
                    onId: req.body.userId,
                    onModel: req.body.userRole === 'employer' ? 'employerProfile' : 'userDetail' // this will take a value as per the followUnFollowRole if followUnFollowRole === 'employer' then onModel = employerProfile elseif followUnFollowRole === 'student' then onModel = userDetail       
                }
            }
        }, { new: true }).exec((error, user) => {
            // console.log('userfollowerr -> ',user.connections);
            if (error || !user._id ) {
                return res.status(400).json({
                    error: error
                })

            }
            // console.log(`connections users -> ${user.userName}`);
            // res.status(200).json({ user, message: `You are now connected to ${user.userName} ` });
            
            res.status(200).json({ success: true, message: `You are now connected to this user ` });
            // next();
        })

    } else {
        res.status(417).json({ error: 'Please give proper details !!' })
    }
 
}


// UnFollowing function  
// the UnFollowing can be any candidate or employer  
exports.userUnFollowing = (req, res, next) => {
    /* info
    // req.body.userId ===>  authenticated 
    // req.body.userRole ===> authenticated
    // req.body.followUnFollowId ===> authenticater can Following
    // req.body.followUnFollowRole ===> authenticater can Following
    */
    if (req.body.userId && req.body.userRole && req.body.followUnFollowId && req.body.followUnFollowRole) {
        let userDetailDB; // this veriable use to store user db info on besis of which role (student or employer)
        if (req.body.userRole === 'student') {
            userDetailDB = candidateDetailModel; // if user is ***student*** 
        }
        if (req.body.userRole === 'employer') {
            userDetailDB = employerDetailModel;
        }
        userDetailDB.findOne({ _id: req.body.userId }).exec((error, user) => {
            try {
                if (error) {
                    // console.log(error);
                    res.status(404).json({ error: 'You are not ragister user !!' })
                }
                // check the followUnFollowId is here or not  
                if (user.connections.length > 0) {
                    let trueOrfalse = user.connections.map((t, i) => t.onId == req.body.followUnFollowId)

                    // console.log('trueOrfalse1 -> ', trueOrfalse);
                    // console.log('user.connections -> ', user.connections);
                    if (trueOrfalse.includes(true)) {
                        // console.log('trueOrfalse.includes(true) -> ', trueOrfalse.includes(true));
                        // console.log('Yes follow this user !!');
                        userDetailDB.findByIdAndUpdate(req.body.userId, {
                            $pull: {
                                connections: {
                                    onId: req.body.followUnFollowId,
                                    onModel: req.body.followUnFollowRole === 'employer' ? 'employerProfile' : 'userDetail' // this will take a value as per the followUnFollowRole if followUnFollowRole === 'employer' then onModel = employerProfile elseif followUnFollowRole === 'student' then onModel = userDetail       
                                }
                            }
                        }, (error, user) => {
                            if (error) {
                                return res.status(400).json({
                                    error: error
                                })

                            }
                            
                        })

                    }
                    else if (trueOrfalse.includes(false)) {
                        // console.log('You are not following this user !!');
                        return res.status(208).json({ message: 'You are not following this user !!' })
                    }

                }
                if (user.connections.length == 0) {
                    // console.log('You are not following any user !!');
                    return res.status(208).json({ message: 'You are not following any user !!' })
                }
                next();
            } catch (error) {
                console.log('error -> catch 199 ', error.message);
            }

        });



    } else {
        res.status(417).json({ error: 'Please give proper details !!' })
    }

}

// UnFollower function  
// the UnFollower can be any candidate or employer  
exports.userUnFollower = (req, res, next) => {
    /* info
    // req.body.userId ===>  authenticated 
    // req.body.userRole ===> authenticated
    // req.body.followUnFollowId ===> authenticater can Following
    // req.body.followUnFollowRole ===> authenticater can Following
    */
    if (req.body.userId && req.body.userRole && req.body.followUnFollowId && req.body.followUnFollowRole) {
        let userDetailDB; // this veriable use to store user db info on besis of which role (student or employer)
        if (req.body.followUnFollowRole === 'student') {
            userDetailDB = candidateDetailModel; // if user is ***student*** 
        }
        if (req.body.followUnFollowRole === 'employer') {
            userDetailDB = employerDetailModel;
        }
        userDetailDB.findByIdAndUpdate(req.body.followUnFollowId, {
            $pull: {
                connections: {
                    onId: req.body.userId,
                    onModel: req.body.userRole === 'employer' ? 'employerProfile' : 'userDetail' // this will take a value as per the followUnFollowRole if followUnFollowRole === 'employer' then onModel = employerProfile elseif followUnFollowRole === 'student' then onModel = userDetail       
                }
            }
        }, { new: true }).exec((error, user) => {
            if (error) {
                return res.status(400).json({
                    error: error
                })

            }
            // console.log(`not connections users -> ${user.userName}`);
            // res.status(200).json({ user, message: `You are now disconnect connected to ${user.userName} ` });
            return res.status(200).json({ success: true, message: `You are now disconnect connected from this user` });
            // next();
        })

    } else {
        res.status(417).json({ error: 'Please give proper details !!' })
    }

}
