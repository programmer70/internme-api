/* 
@dec = show post
@access = public
@path = /show
*/

// import module or external functions
const Employer_postModel = require('../../model/hrModel/post');
const Candidate_postModel = require('../../model/caditateModel/post');

// show post by params 
exports.showPostDparams = (req, res, next) => { // show post details by params id
     let postModel;  //for selecting database according to userType(employer or Candidate)
     postModel = req.params.postrole === 'employer' ? Employer_postModel : Candidate_postModel;  /// salect the post Model
     if (req.params.postId) {
          // console.log('model -> ',postModel,' id -> ',req.params.postId);
          postModel.findById(req.params.postId).exec((error, result) => {
               // console.log('error -> ',error,' result -> ',result);
               if (error || !result) {
                    // console.error('error log ->',error);
                    return res.status(402).json({ error: "Post not found !!!" });
               } else {
                    // console.log('resutl ',result);
                    req.postInfo = result
                    req.postModel = req.params.postrole === 'employer' ? 'EmployerPost' : 'CandidatePost';
                    // return res.status(200).json(result);
               }
               next();
          })
     } else {
          console.error('error -> postId require in URL');
     }
}


/** @path => /all 
     @dec => this will show all post
*/
// show all post  
exports.showAllpost = (req, res, next) => {
     let mainResult = [];  //in this list marge both conadidate and employer post list 
     // const { token } = req.cookies;
     // console.log('token -> ', token);

     if (req.auth.payload.id2) {
          Employer_postModel.find().populate('postedBy').sort({ createdAt: -1 }).then(result => {
               mainResult = [...result] // marge the list (using sprid oprater)
          }).catch(error => {
               console.error('error cathed -> ', error);
               return res.status(402).json({ error: 'somthing goes wrong please try again later !!' })
          })
          Candidate_postModel.find().populate('postedBy').sort({ createdAt: -1 }).then(result => {
               mainResult = [...mainResult, ...result] // marge list (using sprid oprater)
               // console.log('result -> ', mainResult);
               return res.status(200).json({ result: mainResult })

          }).catch(error => {
               console.error('error catched -> ', error);
               return res.status(402).json({ error: 'somthing goes worng please try again later !!!' })
          })
     } else {
          console.error('error log --> must have authrizations for this oparation');
          return res.status(401).json({ error: "you have no authorization for this oparation" });
     }

}

/**
 * 
 * @param {postId} req post id must important for find post
 * @param {result} res res will give the resutl to frant-end
 * @param {nall} next no use as of now
 * @dec --> this functions will find the post according  to postId
 * @access --> Private
 * @path   --> show/:postId-:postrole --> postrole is the user type Candidate or employer
 */
// show sing post according to it's id
exports.showPostById = (req, res, next) => {
     let postModel;  //for selecting database according to userType(employer or Candidate)
     postModel = req.params.postrole === 'employer' ? Employer_postModel : Candidate_postModel;  /// salect the post Model
     if (req.params.postId) {
          // console.log('model -> ',postModel,' id -> ',req.params.postId);
          postModel.findById(req.params.postId).populate('like.user', '', 'userDetail').populate('comments').populate('postedBy').exec((error, result) => {
               // console.log('error -> ',error,' result -> ',result); 
               if (error || !result) {
                    // console.error('error log ->',error);
                    return res.status(402).json({ error: "Post not found !!!" });
               } else {
                    // console.log('resutl ',result);
                    return res.status(200).json(result);
               }
          })
     } else {
          console.error('error -> postId require in URL');
     }
}