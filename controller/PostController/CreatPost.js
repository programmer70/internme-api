/* 
@dec = creat post
@access = PRIVATE
@path = /creatPost/:id-:role
*/

const Candidate_detailmodel = require('../../model/caditateModel/ragisterDetail');
const Employer_detailmodel = require('../../model/hrModel/ragisterDetail');
const Employer_postModel = require('../../model/hrModel/post');
const Candidate_postModel = require('../../model/caditateModel/post');

exports.userDinfo = async (req, res, next, id) => {   // user detail information
     let userModel; // this will Candidate_detailmodel or Employer_detailmodel
     if (id) {
          if (req.params.role) {
               userModel = req.params.role === 'employer' ? Employer_detailmodel : Candidate_detailmodel;

               // console.log('id - ', id);
               // console.log('role - ', req.params.role);
               // console.log('userModel - ', userModel);

               userModel.findById(id).populate('user', '_id role')
                    .exec((error, user) => {
                         // console.log('line 9 => ', user)
                         if (error || !user) {
                              return res.status(404).json({ error: 'User not found --- !!' })
                         }
                         // console.log('user -> ',user);
                         req.userDetialInfo = user  // add all user info in userDetialInfo obj
                         req.userRole = req.params.role
                         next();
                    })
          }
          else {
               return res.status(401).json({ error: 'Please fill proper params' })
          }

     } else {
          return res.status(401).json({ error: 'Id require on params' })
     }

}


exports.CreatPost = async (req, res, next) => {
     // console.log('this is creatPost');
     // console.log('body => ', req.body);
     let NewpostModel;
     try {
          if (req.userRole) {
               if (req.body.postType && req.body.visible) {
                    NewpostModel = req.userRole === 'employer' ? Employer_postModel : Candidate_postModel;
                    let newPost = await new NewpostModel(req.body);

                    newPost.postedBy = req.userDetialInfo;
                    newPost.save(async (error, result) => {
                         if (error) {
                              console.log('error ....', error);
                              return res.status(400).json(error);
                         } else {
                              console.log('success .///...');
                              return res.status(200).json({ success: true, message: 'New post created succefully !!',post:result })
                         }
                    })
               } else {
                    console.error('pleas fill proper details');
                    return res.status(401).json({ error: 'pleas fill proper details' })
               }

               // console.log('this is user --> ', req.userDetialInfo);
          }
     } catch (error) {
          console.log('error catch while creating new post');
          res.status(400).json({ error: 'somthing is wrong pleas try again !!' })
     }
}