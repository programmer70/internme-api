// post model 
const Candidate_postModel = require("../../../model/caditateModel/post");
const Employer_postModel = require("../../../model/hrModel/post");
// detailmodel 
const Candidate_detailmodel = require("../../../model/caditateModel/ragisterDetail");
const Employer_detailmodel = require("../../../model/hrModel/ragisterDetail");
const comments = require("../../../model/comments");

// creat comment 
exports.addComment = async (req, res, next) => {
     console.log('body -> ', req.body); // want comment userId userrole and postid and role
     // console.log('auth -> ',req.auth); // verify 2 
     let postModel, userModel;
     if (req.postModel && req.userRole && req.body.text) {
          // console.log('pmodel and usemodel ', req.postModel && req.userRole);
          postModel = req.postModel === 'EmployerPost' ? Employer_postModel : Candidate_postModel;  /// salect the post Model
          userModel = req.userRole === 'employer' ? Employer_detailmodel : Candidate_detailmodel;

          // check the postId  then move farther
          await postModel.findOne({ _id: req.params.postId }).exec((error, result) => {
               try {
                    if (error) {
                         console.log(error);
                         res.status(404).json({ error: 'error while find the post !!' })
                    } else {
                         // if user 

                         comments.find({ user: req.params.detailId }).countDocuments((error, count) => console.log('error -> ', error, ' ', 'count -> ', count));

                         let Newcomment = new comments(req.body)
                         Newcomment.user = req.userDetialInfo;
                         Newcomment.userModel = req.userRole === 'employer' ? 'employerProfile' : 'userDetail'
                         Newcomment.post = req.postInfo;
                         Newcomment.onModel = req.postModel
                         Newcomment.save((error, result) => {
                              if (error) {
                                   return res.status(403).json({ error: 'error while saving the comment', success: false })
                              } if (!error && result) {

                                   // console.log('result -> ', result);
                                   postModel.findByIdAndUpdate(req.params.postId, {
                                        $push: {
                                             comments: result,
                                        }
                                   }, { new: true }, (error, postResult) => {
                                        if (error || !postResult) {
                                             return res.status(402).json({ success: false, error: 'somthing error wile push the data in post comments' })
                                        } if (!error) {
                                             userModel.findByIdAndUpdate(req.params.detailId, {
                                                  $push: {
                                                       comments: result
                                                  }
                                             }, { new: true }).exec((error, user) => {
                                                  console.log('comment -> ', user.comments)
                                                  if (error) {
                                                       return res.status(402).json({ success: false, error: 'somthing error wile push the data in user comments' })
                                                  }
                                                  return res.status(200).json({ success: true, message: 'success fully comment this post', comments: user.comments, postResult, result })
                                             })
                                        }
                                   })
                              }
                         })
                    }
                    // return res.status(200).json({ message: "you can comment this post ??" })

               } catch (error) {
                    console.error('error -> comment.js catch 108 ', error);
                    return res.status(402).json({ success: false, error: 'somthing serious contect developer !!' })
               }
          });
     }
     else {
          console.error('error !! req.postModel and req.userRole and req.body.text in comment.js');
          return res.status(402).json({ success: false, error: 'somthing serious contect developer !!' })
     }
}

// show comments 
exports.showComments = async (req, res, next) => {

     console.log('body -> ', req.body); // want comment userId userrole and postid and role
     // console.log('auth -> ',req.auth); // verify 2 
     let postModel;
     if (req.postModel) {
          // console.log('pmodel and usemodel ', req.postModel && req.userRole);
          postModel = req.postModel === 'EmployerPost' ? Employer_postModel : Candidate_postModel;  /// salect the post Model
          // check the postId  then move farther
          await postModel.findOne({ _id: req.params.postId }).exec((error, result) => {
               try {
                    if (error) {
                         console.log(error);
                         res.status(404).json({ error: 'error while find the post !!' })
                    } else {
                         // show comments
                         comments.find({ post: req.params.postId }).populate('user').populate('post').exec((error, comments) => {
                              if (error) {
                                   return res.status(402).json({ success: false, error: 'somthing error wile finding the comment data' })
                              } if (!error) {
                                   return res.status(200).json({ success: true, message: 'success to show comment', comments })
                              }

                         })
                    }
                    // return res.status(200).json({ message: "you can comment this post ??" })

               } catch (error) {
                    console.error('error -> comment.js catch 108 ', error);
                    return res.status(402).json({ success: false, error: 'somthing serious contect developer !!' })
               }
          });
     }
     else {
          console.error('error !! req.postModel and req.userRole and req.body.text in comment.js');
          return res.status(402).json({ success: false, error: 'somthing serious contect developer !!' })
     }
}