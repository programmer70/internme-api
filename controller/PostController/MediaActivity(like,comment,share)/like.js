// post model 
const Candidate_postModel = require("../../../model/caditateModel/post");
const Employer_postModel = require("../../../model/hrModel/post");
// detailmodel 
const Candidate_detailmodel = require("../../../model/caditateModel/ragisterDetail");
const Employer_detailmodel = require("../../../model/hrModel/ragisterDetail");

exports.PostLike = async (req, res, next) => {
     console.log('body -> ', req.body); // want likes userId userrole and postid and role
     // console.log('auth -> ',req.auth); // verify 2 
     let postModel, userModel;
     if (req.postModel && req.userRole) {
          // console.log('pmodel and usemodel ', req.postModel && req.userRole);
          postModel = req.postModel === 'EmployerPost' ? Employer_postModel : Candidate_postModel;  /// salect the post Model
          userModel = req.userRole === 'employer' ? Employer_detailmodel : Candidate_detailmodel;

          // check the userId and followUnFollowId  then move farther
          await postModel.findOne({ _id: req.params.postId }).exec((error, result) => {
               try {

                    if (error) {
                         console.log(error);
                         res.status(404).json({ error: 'error while find the post !!' })
                    }
                    // console.log('result.likes -> ', result.likes)
                    // console.log('result.likes -> ', result.likes.length);

                    // if the followUnFollowId is not here then add it 
                    if (result.likes.length >= 0) {
                         let trueOrfalse = result.likes.map((t, i) => t.user == req.params.detailId)
                         // console.log('trueOrfalse1 -> ', trueOrfalse);
                         if (trueOrfalse.includes(true)) {
                              // if user already likes the post then remove it
                              // console.log('pulling the data');
                              // console.log('trueOrfalse.includes(true) -> ', trueOrfalse.includes(true));
                              // console.log('already follow this result !!');
                              try {
                                   postModel.findByIdAndUpdate(req.params.postId, {
                                        $pull: {
                                             likes: {
                                                  user: req.userDetialInfo,
                                                  onModel: req.userRole === 'employer' ? 'employerProfile' : 'userDetail'
                                             }
                                        }
                                   }, { new: true }).exec((error, result) => {
                                        if (error || !result) {
                                             return res.status(402).json({ success: false, error: 'somthing error wile pull the data in post likes' })
                                        } if (!error) {
                                             userModel.findByIdAndUpdate(req.params.detailId, {
                                                  $pull: {
                                                       likedPost: {
                                                            posts: req.postInfo,
                                                            onModel: req.postModel
                                                       }
                                                  }
                                             }, { new: true }).exec((error, user) => {
                                                  if (error) {
                                                       return res.status(402).json({ success: false, error: 'somthing error wile pull the data in user likepost' })
                                                  } if (!error) {
                                                       console.log('success to pull likes data');
                                                       return res.status(200).json({ dislike: true, success: true, message: 'success fully remove likes from post', user, result })
                                                  }
                                             })
                                        }
                                   })
                                   // return res.status(208).json({ error: 'You are already liked this post !!' })
                              } catch (error) {
                                   console.log('error -> likes.js catch 67 ', error.message);
                              }


                         }

                         // console.log('trueOrfalse.includes(false) -> ', trueOrfalse.includes(false));
                         if (!trueOrfalse.includes(true)) {
                              // if user not likes the post then add it
                              console.log('this is push');
                              try {
                                   postModel.findByIdAndUpdate(req.params.postId, {
                                        $push: {
                                             likes: {
                                                  user: req.userDetialInfo,
                                                  onModel: req.userRole === 'employer' ? 'employerProfile' : 'userDetail'
                                             }
                                        }
                                   }, { new: true }, (error, result) => {
                                        if (error || !result) {
                                             return res.status(402).json({ success: false, error: 'somthing error wile push the data in post likes' })
                                        } if (!error) {
                                             userModel.findByIdAndUpdate(req.params.detailId, {
                                                  $push: {
                                                       likedPost: {
                                                            posts: req.postInfo,
                                                            onModel: req.postModel
                                                       }
                                                  }
                                             }, { new: true }).exec((error, user) => {
                                                  if (error) {
                                                       return res.status(402).json({ success: false, error: 'somthing error wile push the data in user likepost' })
                                                  }
                                                  return res.status(200).json({ like: true, success: true, message: 'success fully likes this post', user, result })
                                             })
                                        }
                                   })
                                   // return res.status(200).json({ message: "you can likes this post ??" })
                              } catch (error) {
                                   console.log('error -> likes.js catch 103 ', error.message);
                              }
                         }
                    }
               } catch (error) {
                    console.log('error -> likes.js catch 108 ', error.message);
               }
          });
     }
     else {
          console.log('error !! req.postModel and req.userRole in likes.js');
     }
}
