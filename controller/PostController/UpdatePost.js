/* 
@dec = Update post
@access = PRIVATE
@path = update-info
*/

// import module
const Employer_postModel = require('../../model/hrModel/post');
const Candidate_postModel = require('../../model/caditateModel/post');

/// update the post 
exports.UpdatePost = (req, res, next) => {
     let postModel;
     console.log("body -> ", req.body);
     postModel = req.params.postrole === 'employer' ? Employer_postModel : Candidate_postModel;  // selecte the  post Model (database) according to Role(student or employer) params
     try {
          if (req.params.postId) {
               postModel.findOne({ _id: req.params.postId }).exec((error, profile) => {
                    if (profile) {
                         // console.log('postBy - ', profile.postedBy, ' auid - ', req.auth.payload.id2);
                         let createdOrnot = profile.postedBy == req.auth.payload.id2 ? true : false; // post created or not
                         // console.log('createdOrNot -> ', createdOrnot);
                         if (!createdOrnot) {
                              return res.status(402).json({ error: "You are not created this post !!!" })
                         } if (createdOrnot) {
                              postModel.findOneAndUpdate(
                                   { _id: req.params.postId },
                                   { $set: req.body },
                                   { new: true }
                              ).populate('postedBy').exec((error, result) => {
                                   if (error) {
                                        console.error('error is => ', error);
                                        return res.status(402).json({ error: "post not update Sorry !!" })
                                   } else {
                                        return res.status(200).json({ success: true, message: "post Updated successfully !!", result })
                                   }
                              })
                         }
                         // return res.status(200).json({ resulte: profile })
                    } else {
                         console.log("error=> ", error);
                         return res.status(404).json({ error: "Post not found !!" })
                    }
               })

          }
     } catch (error) {
          console.error('error catch while update the post -> ',error);
          return res.status(403).json({error:"error catch please try again later !!!"})
     }

}