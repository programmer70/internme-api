const userSingupModel = require('../../../model/person')
const userDetailModel = require('../../../model/caditateModel/ragisterDetail')

// exports.singupinfo = async (req, res, next, id) => {
//     console.log('id - ', id);
//     // let token = await req.params.token

//     userSingupModel.findById(id)
//         .exec((error, user) => {
//             // console.log('line 7 => ', user)
//             if (error || !user) {
//                 return res.status(404).json({ error: 'unUser Not Fod !!' })
//             }
//             // console.log('user -> ',user);
//             req.userInfo = user  // add all admin info in adminprofile obj
//             next();
//         })
// }


// create the candidate details 
exports.newCanditateDetail = async (req, res, next) => {
    // console.log('this is cpa => ', req.body.headline)
    try {
        var uid = await ("0000" + ((Math.random() * Math.pow(36, 4)) | 0).toString(36)).slice(-4);  // for creating some uniq alphabet

        let exist = await userSingupModel.findOne({ token: req.params.token, _id: req.params.id })
        let id = await req.userInfo._id;
        // console.log('find -> ', );
        if (exist) {
            if (req.userInfo.detail === true) {
                return res.status(200).json({ success: true, massage: 'Your Profile is already created !!' })
            }
            else if (req.userInfo.detail === false) {
                let newCanditate = await new userDetailModel(req.body);

                newCanditate.user = req.userInfo;
                newCanditate.uniq = uid;
                newCanditate.save(async (error, result) => {
                    if (error) {
                        console.log('error ....', error);
                        return res.status(400).json(error);
                    } else {
                        console.log('success .///...');
                        await userSingupModel.updateOne({ _id: id }, { $set: { detail: true } }, { new: true })

                        return res.status(200).json({ success: true, massage: 'profile created succefully !!' })
                    }
                })
            }
        } else {
            return res.status(404).json({ error: 'User not found !!' })
        }
    } catch (error) {
        res.status(400).json({ error: 'Somthing is wrong please try again' })
    }
}

/*
@router /userDetails/update
@desc   update the old user Details
@acces  PRIVET
@method PUT
*/
exports.updateCanditateDetail = async (req, res, next) => {
    let exist = await userDetailModel.findOne({ _id: req.auth.payload.id2, user: req.auth.payload.id })
    if (exist) {
        //Do database stuff
        userDetailModel.findOne({ _id: req.auth.payload.id2 })
            .then(async (profile) => {
                if (profile) {
                    await userDetailModel.findOneAndUpdate(
                        { _id: req.auth.payload.id2 },
                        { $set: req.body },
                        { new: true }
                    )
                        .then(profile => {
                            profile.updateAt = Date()
                            return res.status(200).json({ success: true, massage: 'update Succefully !!' })

                        })
                        .catch(err => console.log("problem in update" + err));
                } else {
                    return res.status(404).json({ error: 'User not found  !!' })
                }
            })
            .catch(err => console.log("Problem in fetching profile" + err));
    } else {
        return res.status(404).json({ error: 'User Not Found !!' })
    }
}

// exports.checkUser = async (req, res, next) => {

//     let exist = await userSingupModel.findOne({ token: req.params.token, _id: req.params.id })
//     if (exist) {
//         if (req.userInfo.detail && req.userInfo.detail === true) {
//             return res.status(202).json({ detail: true })
//         }
//         if (req.userInfo.detail && req.userInfo.detail === false) {
//             return res.status(202).json({ detail: false, massage: 'please fill detial first' })
//         }
//         return res.status(200).json({ success: true,role:req.userInfo.role })
//     } else {
//         return res.status(404).json({ error: 'User Not Found !!' })
//     }
//     next()
// }

// show single user by it's id
exports.showSingleuser = (req, res, next) => {
    // console.log('user -> ', req.userInfo)
    try {
        userDetailModel.findOne({ user: req.params.id }).populate('user', '_id role detail').select('_id connections user profileImg profileCoverimg userName firstName lastName uniq likedPost comments').exec((error, user) => {
            if (error || !user) {
                return res.status(404).json({ error: 'User no found !!' })
            } else {
                return res.status(200).json({ user })
            }
        })
    } catch (error) {
        console.log('catch the error in single user --', error);
    }

}

// show single user full info by firstNmae lastName and uid
exports.showSingleuserbyName = (req, res) => {
    // console.log('data -> ', req.params.fname, ' ', req.params.lname, ' ', req.params.unid);
    let fname = req.params.fname;
    let lname = req.params.lname;
    let uid = req.params.unid;
    try {
        userDetailModel.findOne({ firstName: fname, lastName: lname, uniq: uid }).populate('user', '_id email role').exec((error, user) => {
            if (error || !user) {
                return res.status(404).json({ error: 'user not found !!' })
            } else {
                // console.log('user -> ',user);
                return res.status(200).json({ user })
            }
        })
    } catch (error) {
        console.log('catch the error in single user by name -- ', error)
    }
}


exports.showAlluser = (req, res, next) => {
    userDetailModel.find().populate('user', '_id role detail').sort({ createdAt: -1 })
        .select('user uniq headline _id userName firstName lastName profileCoverimg profileImg connections')
        .exec((error, users) => {
            if (error) {
                console.log('error => ', error);
                return res.status(403).json({ error: 'some error' })
            }
            // console.log('employerlist ->  ',users);

            return res.status(200).json({ users })

        })
}

/* this is use to show all connections with details 

// exports.showCanditateConnections = async (req, res) => {
//     console.log('id - ', req.body.userId);
//     let allConnecitons = [] // making proper list of obj for map in front-end

//     if (req.body.userId) {
//         await userDetailModel.findOne({ _id: req.body.userId }).populate({
//             path: 'connections', populate: [{

//                 path: 'onId',
//                 model: 'employerProfile' // give the ref model to populate 

//             }]
//         })
//             .exec((error, user) => {
//                 console.log('line 174 => ', user)
//                 if (error || !user) {
//                     console.log('line 176 => ', error)
//                     return res.status(404).json({ error: 'User not found --- !!' })
//                 }
//                 if (!user.connections) {
//                     // if there is no.. connections 
//                     console.log('no connections 1');
//                     return res.status(401).json({ error: "you have no connections" })
//                 }
//                 // console.log('user -> ', user);
//                 // return res.status(200).json({ user: user.connections })
//                 // return allConnecitons.push(user.connections.filter(filterEmpl => filterEmpl.onModel === 'employerProfile'))
//                 user.connections.filter(filterEmpl => { // try to filter employerProfile and push in a list
//                     if (filterEmpl.onModel === 'employerProfile') {
//                         allConnecitons.push(filterEmpl)
//                     }

//                     return allConnecitons
//                 })
//             })
//         await userDetailModel.findOne({ _id: req.body.userId }).populate({
//             path: 'connections', populate: [{

//                 path: 'onId',
//                 model: 'userDetail'

//             }]
//         })
//             .exec((error, user) => {
//                 // console.log('line 174 => ', user)
//                 if (error || !user) {
//                     console.log('line 176 => ', error)
//                     return res.status(404).json({ error: 'User not found --- !!' })
//                 }
//                 // console.log('user -> ', user);
//                 // return res.status(200).json({ user: user.connections })
//                 if (!user.connections) {
//                     console.log('no connections 2');
//                     return res.status(401).json({ error: "you have no connections" })
//                 }
//                 // allConnecitons.push(user.connections.filter(filterEmpl => filterEmpl.onModel === 'userDetail'))
//                 user.connections.filter(filterEmpl => {  // try to filter userDetail and push in a list
//                     if (filterEmpl.onModel === 'userDetail') {
//                         allConnecitons.push(filterEmpl)
//                     }

//                     return allConnecitons
//                 })
//                 // console.log('allconnecitons 202 -> ', allConnecitons);
//                 return res.status(200).json({ user: allConnecitons })
//             })
//     } else {
//         return res.status(402).json({ error: 'Please give proper detials --- !!' })
//     }

// }
*/

// this use for show connections details as per the req.body.dbRef
exports.showCanditateConnections = (req, res) => {
    console.log('id scc - ', req.body);

    if (req.body.dbRef && req.body.userId) {
        userDetailModel.findOne({ _id: req.body.userId }).populate({
            path: 'connections', populate: [{
                path: 'onId',
                model: req.body.dbRef

            }]
        })
            .exec((error, user) => {
                // console.log('line 144 => ', user)
                if (error || !user) {
                    console.log('line 146 => ', error)
                    return res.status(404).json({ error: 'User not found --- !!' })
                }
                // console.log('user -> ', user);
                return res.status(200).json({ user: user.connections.filter(element => element.onId != null && element.onId.user) }) // filter the element. remove null element  
            })
    } else {
        return res.status(402).json({ error: 'Please give proper detials --- !!' })
    }

}